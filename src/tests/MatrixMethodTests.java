package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	
	//test duplicateMethod
	void testDuplicate() {
		
		// when array is a rectangle.
		
		int [][] inputArray1 = {{1,2,3},{4,5,6}};
		
		MatrixMethod matrix1 = new MatrixMethod();
		
		int [][] expected1 = {{1,1,2,2,3,3},{4,4,5,5,6,6}};
		
		int [][] result1 = matrix1.duplicate(inputArray1);
		
		assertArrayEquals(expected1,result1);
		
		
		// when array is a square
		int [][] inputArray2 = {{1,2,3,},{4,5,6},{7,8,9}};
		
		MatrixMethod matrix2 = new MatrixMethod();
		
		int [][] expected2 = {{1,1,2,2,3,3},{4,4,5,5,6,6},{7,7,8,8,9,9}};
		
		int [][] result2 = matrix2.duplicate(inputArray2);
		
		assertArrayEquals(expected2,result2);
		
		
	}

}
