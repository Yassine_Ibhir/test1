package tests;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import automobiles.Car;

class CarTests {

	@Test
	// test the constructor when an exception is thrown.
	void testCar() {

		try {
			Car car1 = new Car(-3);
			fail("The test should throw an exception");
		}

		catch(IllegalArgumentException e){

			System.out.println("The test passes");
		}

		catch(Exception e) {
			fail("The test  throw an exception but not the right one");
		}


	}

	// test getSpeed.
	@Test
	void testSpeed() {

		Car car1 = new Car(23);
		int result1 = car1.getSpeed();
		int expected1 = 23;
		assertEquals(expected1,result1);

		Car car2 = new Car(0);
		int result2 = car2.getSpeed();
		int expected2 = 0;
		assertEquals(expected2,result2);
	}

	// test getLocation and the final static max_position
	@Test
	void testLocation() {

		Car car2 = new Car(23);
		int result = car2.getLocation();
		int expected = 50;
		assertEquals(expected,result);

		// test the max_position 
		int maxP = Car.MAX_POSITION;
		assertEquals(100,maxP);
	}

	@Test

	//test if the car moves right correctly.
	void testMoveRight() {

		// when location + speed > max position
		Car car1 = new Car(40);

		car1.moveRight();

		int result1 = car1.getLocation();

		int expected1 = 90;

		assertEquals(expected1,result1);



		// when location + speed = max position

		Car car2 = new Car(50);

		car2.moveRight();

		int result2 = car2.getLocation();

		int expected2 = 100;

		assertEquals(expected2,result2);

		// when location + speed < max position.

		Car car3 = new Car(51);

		car3.moveRight();

		int result3 = car3.getLocation();

		int expected3 = 100;

		assertEquals(expected3,result3);	

	}

	@Test
	//test if the car moves left correctly.
	void testMoveLeft() {
		// when location - speed < 0
		Car car1 = new Car(51);

		car1.moveLeft();

		int result1 = car1.getLocation();

		int expected1 = 0;

		assertEquals(expected1,result1);



		// when location - speed = 0

		Car car2 = new Car(50);

		car2.moveLeft();

		int result2 = car2.getLocation();

		int expected2 = 0;

		assertEquals(expected2,result2);

		// when location - speed > 0

		Car car3 = new Car(49);

		car3.moveLeft();

		int result3 = car3.getLocation();

		int expected3 = 1;

		assertEquals(expected3,result3);	
	}

	// test acceleration method.
	@Test
	void testAcceleration() {
		Car car = new Car(34);
		car.accelerate();
		int result = car.getSpeed();
		int expected = 35;
		assertEquals(expected,result);	
	}
	// test stop method.
	@Test
	void testStop() {
		Car car = new Car(100);
		car.stop();
		int result = car.getSpeed();
		int expected = 0;
		assertEquals(expected,result);	
	}

}

