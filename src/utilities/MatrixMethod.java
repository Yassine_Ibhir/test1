package utilities;

public class MatrixMethod {

	// duplicates elements in 2d array
	public int [][] duplicate(int [][] arr){

		int [][] dup = new int[arr.length][arr[0].length*2];

		for(int i = 0; i<arr.length;i++) {
			
			int temp=0;// this variable is the index of the duplicate array elements.
			
			for(int j = 0; j<arr[i].length; j++) {
				
				dup[i][temp] = arr[i][j];
				
				temp++;
				
				dup[i][temp] = arr[i][j];	
				
				temp++;

			}
		}

		return dup;
	}
}
